const gulp = require('gulp');
const gutil = require('gulp-util');


/* *************
  Config
************* */

const globalData = {
    data: require('./src/data/data.json')
};


/* *************
  CSS
************* */

const sass = require('gulp-sass')(require('node-sass'));
const postcss = require('gulp-postcss');
const scss = require('postcss-scss');
const autoprefixer = require('autoprefixer');
const postcssProcessors = [
    autoprefixer()
]

gulp.task('sassInline', () => {
    return gulp.src('src/sass/inline.scss')
        .pipe(
           postcss(postcssProcessors, {syntax: scss})
        )
        .pipe(
            sass({ outputStyle: 'expanded' })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/css/'));
    
});

gulp.task('sassEmbedded', () => {
    return gulp.src('src/sass/embedded.scss')
        .pipe(
           postcss(postcssProcessors, {syntax: scss})
        )
        .pipe(
            sass({ outputStyle: 'expanded' })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/css/')); 
});

/* *************
  TEMPLATING
************* */

const nunjucksRender = require('gulp-nunjucks-render');
const data = require('gulp-data');

gulp.task('nunjucks', gulp.series('sassEmbedded', () => {
    return gulp.src('src/emails/*.nunjucks')
        .pipe(
            data(() => {
                return globalData;
            })
            .on('error', gutil.log)
        )
        .pipe(
            nunjucksRender({
                path: ['src/templates/', 'build/css/']
            })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/'));
}));

const inlineCss = require('gulp-inline-css');

gulp.task('inlinecss', gulp.series('sassInline', 'nunjucks', () => {
    return gulp.src('build/*.html')
        .pipe(
            inlineCss({
                applyStyleTags: false,
                removeStyleTags: false
            })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload());
}));


/* *************
    ZIP
************* */

const zip = require('gulp-zip');

gulp.task('zip', () => {
    return gulp.src('build/**')
        .pipe(zip('build.zip'))
        .pipe(gulp.dest('./'));
});


/* *************
	SERVER
************* */

const connect = require('gulp-connect');

gulp.task('connect', (callback) => {
    connect.server({
        port: 8000,
        root: 'build', // Serve from build directory instead,
        livereload:true
    });
    callback();
});



/* *************
    WATCH
************* */

const filesToWatch = [
    'src/sass/**/*.scss',
    'src/emails/*.nunjucks',
    'src/templates/**/*.nunjucks',
    'src/data/*.json'
]

gulp.task('watch', (callback) => {
    gulp.watch(filesToWatch, gulp.series('nunjucks', 'inlinecss')); 
    callback();
});

/* *************
    DEFAULT
************* */

gulp.task('default', gulp.series('connect', 'nunjucks', 'inlinecss', 'watch'));


